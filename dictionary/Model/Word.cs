﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dictionary.Model
{
    public class Word
    {
        public int Id { get; set; }
        public string Meaning { get; set; }
        public string Description { get; set; }  
    }
}
