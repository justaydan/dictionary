﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dictionary.Model
{
    public class Word_Language
    {
        public int Id { get; set; }
        public int Word_Id { get; set; }
        public int Language_Id { get; set; }
        public string Translation { get; set; } 
    }
}
